# frozen_string_literal: true

#
# Class CreateUsers creates user table
#
class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :picture
      t.string :password
      t.timestamps
    end
  end
end
