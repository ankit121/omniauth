# frozen_string_literal: true

#
# Class CreateAuthorizations creates authorization table
#
class CreateAuthorizations < ActiveRecord::Migration[5.2]
  def change
    create_table :authorizations do |t|
      t.string :provider
      t.string :uid
      t.string :username
      t.integer :user_id
      t.string :token
      t.timestamps
    end
  end
end
