# frozen_string_literal: true

require 'test_helper'

#
# Class AuthorizationTest test authorization
#
class AuthorizationTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'find_from_auth' do
    authorization = Authorization.first
    auth = OpenStruct.new(provider: authorization.provider,
                          uid: authorization.uid)
    user = Authorization.find_from_auth(auth)
    assert_not_nil(user, 'user present')
  end

  test 'create_from_auth' do
    auth = OpenStruct.new('provider' => 'github',
                          'uid' => '23324432',
                          'info' => OpenStruct.new(name: 'test-user',
                                                   nikname: 'test',
                                                   email: 'hefwhjf@gmail.com'),
                          credentials: OpenStruct.new(
                            token: 'fkfjhgkfsfjdkjfdkj'
                          ))
    Authorization.create_from_auth(auth, nil)
    assert true
  end
end
