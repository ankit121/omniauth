# frozen_string_literal: true

#
# Class Authorization provides methods to intract with table
#
class Authorization < ApplicationRecord
  belongs_to :user
  validates :user_id, :uid, :provider, presence: true
  validates :uid, uniqueness: { scope: :provider }

  def self.find_from_auth(auth)
    find_by(provider: auth['provider'], uid: auth['uid'])
  end

  def self.create_from_auth(auth, user = nil)
    user ||= User.find_or_create_from_auth_hash(auth)
    create(user_id: user.id, uid: auth.uid, provider: auth.provider,
           token: auth.credentials.token, username: auth.info.nickname)
  end
end
