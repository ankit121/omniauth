# frozen_string_literal: true

# module containing github related apis
module Github
  #
  # Class ApiResponseParser provides base class for parsing
  # required data from github apis
  #
  class ApiResponseParser
    #
    # <extract repo names from github api response>
    #
    # @param [<ostruct>] response <api response from github>
    # @param [<string>] key <name of key to be extract from response>
    #
    # @return [<Array>] <Array of string containing repo names>
    #
    def self.extract_names(response, key)
      response.map { |k| k[key.to_sym] }
    end
  end
end
