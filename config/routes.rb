# frozen_string_literal: true

Rails.application.routes.draw do
  # get 'user/show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#show'
  get 'login', to: redirect('auth/:provider/callback'), as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'auth/:provider/callback', to: 'sessions#create', as: :session
  get 'auth/failure', to: redirect('/')
  get 'home', to: 'home#show'
  get 'profile', to: 'users#show', as: 'profile'

  resource :user do
    get :respositories, on: :member
    get 'repo/:name', to: 'users#repo_detail', as: :repo_detail
    get 'repo/:name/commits', to: 'users#repo_commits', as: :repo_commits
  end
end
