# frozen_string_literal: true

#
# Class ApplicationController provides base class for controllers
#
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user

  #
  # <check user authorization>
  #
  def authenticate
    redirect_to :login unless user_signed_in?
  end

  #
  # <find user from session >
  #
  #
  # @return [<object>] <current user from database>
  #
  def current_user
    @current_user = User.find_by(id: session[:user_id]) if session[:user_id]
  end

  #
  # <check wether user is signed in or not>
  #
  #
  # @return [<boolean>] <description>
  #
  def user_signed_in?
    # converts current_user to a boolean by negating the negation
    !current_user.nil?
  end
end
