# frozen_string_literal: true

require 'test_helper'

#
# Class UserTest tests user model
#
class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'find_or_create_from_auth_hash' do
    auth = OpenStruct.new('provider' => 'github',
                          'uid' => '23324432',
                          'info' => OpenStruct.new(name: 'test-user',
                                                   nikname: 'test',
                                                   email: 'hefwhjf@gmail.com'),
                          credentials: OpenStruct.new(
                            token: 'fkfjhgkfsfjdkjfdkj'
                          ))
    User.find_or_create_from_auth_hash(auth)
  end
end
