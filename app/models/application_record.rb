# frozen_string_literal: true

#
# Class ApplicationRecord provides base class for all models
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
