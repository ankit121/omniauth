# frozen_string_literal: true

module SignInHelper
  def sign_in_as(auth)
    post session_path("provider": auth.provider, "uid": auth.uid)
  end
end
