# frozen_string_literal: true

#
# Class HomeController provides root actions
#
class HomeController < ApplicationController
  def show; end
end
