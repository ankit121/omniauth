# frozen_string_literal: true

#
# Class User provides methods to intract with users table
#
class User < ApplicationRecord
  has_many :authorizations, dependent: :destroy
  def self.find_or_create_from_auth_hash(auth)
    includes(:authorizations)
      .where(authorizations: { provider: auth.provider, uid: auth.uid })
      .first_or_initialize.tap do |user|
        fill_attributes(user, auth)
      end
  end

  def self.fill_attributes(user, auth)
    first, last = auth.info.name.split('-')
    user.first_name = first
    user.first_name = last
    user.email = auth.info.email
    user.picture = auth.info.image
    user.save!
  end
end
