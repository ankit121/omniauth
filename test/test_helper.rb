# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

module ActiveSupport
  # class for test config
  class TestCase
    # Setup all fixtures in test/fixtures/*.yml for all tests in order.
    fixtures :all

    def setup_omniauth_mock(user)
      authorization = user.authorizations.find_by(provider: 'github')
      OmniAuth.config.test_mode = true
      OmniAuth.config.add_mock(:github, omniauth_hash(user, authorization))
      Rails.application.env_config['omniauth.auth'] = OmniAuth.config.mock_auth[
                                                        :github]
    end

    # Add more helper methods to be used by all tests here...
    def login_with_user(user)
      setup_omniauth_mock(user)
      get session_path(:github)
    end

    def omniauth_hash(user, authorization)
      { 'provider' => 'github',
        'uid' => authorization.uid,
        'info' => { 'name' => user.first_name,
                    'email' => user.email },
        credentials: {
          token: authorization.token
        } }
    end

    # Add more helper methods to be used by all tests here...
  end
end
