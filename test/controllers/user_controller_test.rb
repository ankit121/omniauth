# frozen_string_literal: true

require 'test_helper'

#
# Class UserControllerTest provides tests Usercontroller
#
# @author Joe Blog <Joe.Blog@nowhere.com>
#
class UserControllerTest < ActionDispatch::IntegrationTest
  def setup
    login_with_user(User.first)
  end

  test 'respositories should return repos' do
    get respositories_user_path
    assert_response :success
  end

  test 'repo_detail should return repo details' do
    get repo_detail_user_path(name: 'ruby-server')
    assert_response :success
  end

  test 'repo_detail should return commit info' do
    get repo_commits_user_path(name: 'ruby-server')
    assert_response :success
  end
end
