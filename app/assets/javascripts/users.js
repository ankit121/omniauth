$(document).on('turbolinks:load', function() {
  // $('#start_date').datepicker({});
  // $('#end_date').datepicker({});
  var data = $("#c_data").val();
  if (data != null) {
    data = JSON.parse(data);

    margin = { top: 20, right: 20, bottom: 100, left: 40 },
    width = 600 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom,

    parseDate = d3.time.format("%Y-%m-%d").parse,
    x = d3.scale.ordinal().rangeRoundBands([0, width], .5),
    y = d3.scale.linear().range([height, 10]),

    d3_max = d3.max(data, function(d) { return d.count; })
    y_max = d3_max > 4 ? d3_max : 4
    xAxis = d3.svg.axis()
              .scale(x)
              .orient("bottom")
              .tickFormat(d3.time.format("%Y-%m-%dT%H:%M")),

    yAxis = d3.svg.axis()
              .scale(y)
              .orient("left")
              .ticks(y_max),

    svg = d3.select(".chart")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    data.forEach(function(d) {
      d.date = parseDate(d.date);
      d.count = +d.count;
    });

    x.domain(data.map(function(d) {
      return d.date;
    }));

    y.domain([0, y_max]);

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll("text")
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", "-.55em")
      .attr("transform", "rotate(-90)")
      .style("text-anchor", "end")

    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Date ($)");

    svg.selectAll("bar")
      .data(data)
      .enter().append("rect")
      .style("fill", "steelblue")
      .attr("x", function(d) {
         return x(d.date);
      })
      .attr("width", x.rangeBand())
      .attr("y", function(d) {
         return y(d.count);
      })
      .attr("height", function(d) {
        return height - y(d.count);
      });
  }
});