# frozen_string_literal: true

# github module to wrap github related code
module Github
  #
  # Class GithubApi provides <methods to call and parse github apis>
  #
  class GithubApi
    #
    # <initialize faraday object for performing requests>
    #
    # @param [<activerecord object>] auth <object of auth model>
    #
    def initialize(auth)
      @conn = Faraday.new('https://api.github.com', headers:
                          { 'accept': 'application/json',
                            'content-type': 'application/json',
                            'X-GitHub-Media-Type': 'github.v3' }) do |faraday|
        faraday.authorization(:token, auth.token)
        faraday.response :logger, ::Logger.new(STDOUT), bodies: true
        faraday.adapter Faraday.default_adapter
      end
    end

    #
    # <peroform get request>
    #
    # @param [<url>] uri <url to be hit>
    # @param [<hash>] params <query params>
    #
    # @return [<osstruct>] <response>
    #
    def get(uri, params = nil)
      perform_request('get', "/#{uri}", params.except(:page))
    end

    #
    # <perform post request>
    #
    # @param [<url>] uri <url to be hit>
    # @param [<hash>] params <post request params>
    #
    # @return [<osstruct>] <response>
    #
    def post(uri, params)
      response = perform_request('post', "/#{uri}", params)
      response.errors ? response : response.data
    end

    private

    #
    # <perform faraday request>
    #
    # @param [<string>] method <name of method>
    # @param [<url>] uri <url to be hit>
    # @param [<hash>] data <parameters for post request>
    #
    # @return [<osstruct>] <api response>
    #
    def perform_request(method, uri, data)
      response = @conn.send method, uri.to_s, data
      JSON.parse(response.body, object_class: OpenStruct)
    end
  end
end
