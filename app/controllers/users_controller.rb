# frozen_string_literal: true

#
# Class UsersController for users actions
#
class UsersController < ApplicationController
  before_action :authenticate

  def show; end

  #
  # <fetch the names of repositories of a user>
  #
  #
  # @repo [<Array>] <contaning names of repositories>
  #
  def respositories
    response = Github::GithubApi.new(auth_provider).get('user/repos', {})
    @repos = Github::ApiResponseParser.extract_names(response, 'name')
  end

  #
  # <fetch details of given repository>
  #
  #
  # @repo_info [<hash>] <required info of repository>
  #
  def repo_detail
    response = Github::GithubApi.new(auth_provider).get(repo_url, {})
    @repo_info = Github::RepoDetailsParser.info_hash(response)
  end

  #
  # <fetch commits of a repository>
  #
  #
  # @commit_info [<Array of hash>] <containing required info>
  #
  def repo_commits
    response = Github::GithubApi.new(auth_provider).get(commit_url, date_params)
    @commit_info = Github::RepoDetailsParser.repo_commits(response)
  end

  private

  #
  # <fetch the authentication provider of user>
  #
  #
  # @return [<object>] <Authorization object of current_auth provider>
  #
  def auth_provider
    Authorization.find_by(user_id: @current_user.id, provider: provider_name)
  end

  #
  # <permit repo name in params>
  #
  def repo_name
    @repo_name = params.permit(:name).require(:name)
  end

  #
  # <permit date params>
  #
  def date_params
    @s_date = params[:since]
    @e_date = params[:until]
    params.permit(:since, :until)
  end

  #
  # <current provider name>
  #
  #
  # @return [<string>] <current provider name>
  #
  def provider_name
    'github'
  end

  #
  # <provide url for api commit action>
  #
  #
  # @return [<string, hash>] <url with params>
  #
  def commit_url
    repo_url + '/commits'
  end

  #
  # repo url provide dynamic url
  #
  #
  # @return [<string>] <url sting>
  #
  def repo_url
    "repos/#{auth_provider.username}/#{repo_name}"
  end
end
