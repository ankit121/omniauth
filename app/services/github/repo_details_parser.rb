# frozen_string_literal: true

module Github
  #
  # Class RepoDetailsParser provides <methods for parsing response from github >
  #
  class RepoDetailsParser < ApiResponseParser
    #
    # <make a hash from data of github api>
    #
    # @param [<response>] response <api response>
    #
    # @return [<hash>] <hash of values required>
    #
    def self.info_hash(response)
      {
        name: response[:name],
        description: response[:description],
        language: response[:language],
        default_branch: response[:default_branch],
        created_at: response[:created_at],
        type: response[:private] ? 'private' : 'public',
        updated_at: response[:updated_at]
      }
    end

    #
    # <provide requrired values from commits of a repo>
    #
    # @param [<response>] response <github api response>
    #
    # @return [<array of hash>] <arra containing hash of dates>
    #
    def self.repo_commits(response)
      date_arr = response.map { |k| k.commit.author.date.to_date }
      res_hash = []
      return [] if date_arr.nil?

      date_arr.group_by(&:itself).each do |k, v|
        res_hash << { date: k, count: v.size }
      end
      res_hash
    end
  end
end
