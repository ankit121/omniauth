# frozen_string_literal: true

#
# Class SessionsController provides for users sessions
#
class SessionsController < ApplicationController
  def new; end

  #
  # <create authorization object for current user>
  #
  def create
    @auth = Authorization.find_from_auth(auth_hash)
    if @auth.present?
      @auth = Authorization.create_from_auth(auth_hash, current_user)
    end
    session[:user_id] = @auth.user.id
    redirect_to profile_path
  end

  #
  # <destroy user session>
  #
  def destroy
    session[:user_id] = nil
    redirect_to root_path
  end

  private

  #
  # <fetch authorization hash from omniauth response>
  #
  #
  # @return [<json>] <omniauth response>
  #
  def auth_hash
    request.env['omniauth.auth']
  end
end
